// CRUD Operations

// [ S E C T I O N] Inserting documents (Create)
// Syntax: db.collectionName.insertOne({object})
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python" ],
	department: "none"
});

// Inserting multiple documents
// Syntax: db.collectionName.insertMany([{objectA}, {objectB}]);
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09123456789",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP" ],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09123456789",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass" ],
		department: "none"
	}
]);

// [ S E C T I O N] Finding documents (Read)
/*
	Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
	db.collectionName.findOne({field: value});
		- If multiple documents
*/

// Leabing the search criteria empty will retrieve all the documents
db.users.find();

db.users.find({firstName: "Stephen"});

// Finding documents with multiple parameters
// Syntax: db.collectionName.find({fieldA: valueA, fieldB: valueB});
db.users.find({lastName: "Armstrong", age: 82});

// [ S E C T I O N] Updating documents (Update)
// Updating a single document
// Syntax: db.collectionName.updateOne({criteria}, {$set: {field: value}})
/*
	updateOne will only update the first document that matches the search criteria 
*/

// Document to be updated
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(

	{ firstName: "Test"},
	{
		$set : {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}

);
db.users.find({firstName: "Bill"});

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set: {fieldA:value}})
*/

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}

);

// Replace One
// Can be used if replacing the whole document is needed
db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "09123456789",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
);

// [ S E C T I O N] Deleting documents (Delete)

// Document to be deleted
db.users.insertOne({
	firstName: "test"
});

// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria});
*/
db.users.deleteOne({firstName: "test"});

// Deleting Many
/*
	Syntax:
		db.collectionName.deleteMany({criteria});
*/
db.users.deleteMany({firstName: "Bill"});


// [ S E C T I O N] Advance Queries

db.users.find({
	contact:{
		phone: "09123456789",
		email:"stephenhawking@gmail.com"
	}
});

db.users.find({"contact.email" : "janedoe@gmail.com"});

// Querying an array with exact elements
db.users.find({courses : ["CSS", "JavaScript", "Python"]});

// Querying an array without regard to the order
db.users.find({courses : {$all: ["React", "Python"]}});

// Querying an embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});
db.users.find({
	namearr: {
		namea: "juan"
	}
});
