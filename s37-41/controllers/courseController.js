const Course = require("../models/Course");

const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = reqBody => {

	// MINI ACTIVITY
	// Create /Instantiate a new object for a new Course
	// Stretch goals: return the saved new Course

	let newCourse = new Course(
		{
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
		}
	);

	return newCourse.save()
		.then(course => {
			return true;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
}

/*MINI ACTIVITY*/
/*
	Steps:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses =() => {

	return Course.find({}).then(result => result)
		.catch(err => {

			console.log(err);

			return false;
		});
};

// Retrieve all ACTIVE courses
/*
	Steps:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActiveCourses = () => {

	return Course.find({ isActive : true }).then(result => result)
		.catch(err => {

			console.log(err);

			return false;
		})
};

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = reqParams => {

	return Course.findById(reqParams.courseId).then(result => result).catch(err => {

			console.log(err)

			return false;
		})
};

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
		.then(course => true).catch(err => {

			console.log(err);

			return false;
		})
}

// A C T I V I T Y S40

module.exports.archiveCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let archivedCourse = {
		isActive : reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
		.then(course => true).catch(err => {

			console.log(err);

			return false;
		})
}
