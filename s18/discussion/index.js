// console.log("Hello World");


/*function printInput(){

	let nickname = prompt("Enter your nickname:");
	return "Hi, " + nickname;
};

printInput();*/

// parameters with the prompt don't need hardcoded arguments since the value will be provided via prompt
function printInput(nickname){

	nickname = prompt("Enter your nickname:");
	return "Hi, " + nickname;
};

// console.log(printInput());

/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

	"name" is called a parameter.

	A "parameter" acts as a named variable/container that exists only inside of a function
	It is used to store information that is provided to a function when it is called/invoked.
*/

function printName(name){
	return "My name is " + name;
};

/*
	the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
*/

console.log(printName("Rusty"));

// variables can be also passed as an argument
let anotherName = "Mitsuha";
console.log(printName(anotherName));


function checkDivisibilityby8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " dvided by 8 is: " + remainder);

	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleby8);
}

checkDivisibilityby8(64);
checkDivisibilityby8(28);

// Functions as Arguments
/*
	Function parameters can also accept other functions as arguments
	Some complex functions use other functions as arguments to perform more complicated results.
	This will be further seen when we discuss array methods.
*/

function tryMe(){
	console.log("Gulat ka noh?");
};

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
};

function invokeFunction(args){
	args();
}

invokeFunction(tryMe);
console.log(argumentFunction);

// Using Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firtName, middleName, lastName){
	return firtName + " " + middleName + " " + lastName;
}

console.log(createFullName("Juan", "Dela", "Cruz", "Jr."));


