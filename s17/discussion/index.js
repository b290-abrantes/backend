// console.log("Hello");

// [ S E C T I O N ] FUNCTIONS
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	// Function Declaration
	//(function statement) defines a function with the specified parameters.
	/*
		function functionName(){
			statement;
			statement;
			statement;
		};
	*/

	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

	function printName(){
		console.log("My name is Rusty.");
	};


// [ S E C T I O N ] FUNCTION INVOCATION
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".
	printName();

	// printAge(); - Uncaught ReferenceError: printAge is not defined, (functions should be declared)

// [ S E C T I O N ] FUNCTION DECLARATIONS VS EXPRESSIONS
	// Function Declarations
	//A function can be created through function declaration by using the function keyword and adding a function name.
	
	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

		declaredFunction(); // declared functions can be hoisted, as long as the function has been defined

		//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

		function declaredFunction() {
			console.log("Hello from the other side.");
		};

		declaredFunction()

	// Function Expression
		//A function can also be stored in a variable. This is called a function expression.
		//A function expression is an anonymous function assigned to the variableFunction

		// Anonymous Function - function without a name

		// variableFunction(); Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

		let variableFunction = function() {
			console.log("I must have called a thousand times.");
		};

		variableFunction();

		//A function expression of a function named func_name assigned to the variable funcExpression
		//How do we invoke the function expression?
		//They are always invoked (called) using the variable name.
		let funcExpression = function funcName() {
			console.log("To tell you I'm sorry.");
		};

		funcExpression();

		//You can reassign declared functions and function expressions to new anonymous functions.

		declaredFunction = function(){
			console.log("updated declaredFunction");
		};

		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression");
		};

		funcExpression();

		const constantFunc = function(){
			console.log("Initialized with const!");
		};

		constantFunc();

		// Uncaught TypeError: Assignment to constant variable.
/*		constantFunc = function(){
			console.log("Try lang");
		};

		constantFunc();
*/

// [ S E C T I O N ] Function Scoping
/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/
	// Local/Block Scope
	{
		let localVar = "Armando Perez";
	};

	let globalVar = "Mr. WorldWide";

	console.log(globalVar);
	// console.log(localVar); Uncaught ReferenceError: localVar is not defined

	// Function Scope
	function showNames(){
		// Function scoped variables
		var functionVar = "Arthas";
		const functionConst = "Porthas";
		let functionLet = "Aramis";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};

	showNames();

	/* all results to error as not defined */
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


	// Nested Function
		// another function inside a function

	function myNewFunction(){
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		};
		// console.log(nestedName); // Uncaught ReferenceError: nestedName is not defined at myNewFunction
		nestedFunction();
	}

	myNewFunction();
	// nestedFunction();

	// Function and Global Scoped Variables
		// Global Scoped Variable

		let globalName = "Bill Gates";

		function myNewFunction2(){
			let nameInside = "Steve Jobs";

			console.log(globalName);

			function myNewFunction2(){
				let nameInside = "Steve Jobs";

				console.log(globalName);
			};
			myNewFunction2();
		};

		myNewFunction2();
		// console.log(nameInside); function scoped cannot be accessed globally;

// [ S E C T I O N ] USING ALERT()
	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

	// alert("Hello");

	function showSampleAlert(){
		alert("Hello, User!");
	};

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");
	//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing

// [ S E C T I O N ] Using prompt()
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

	// let samplePrompt = prompt("Enter your name.");

	// console.log(typeof samplePrompt);
	// console.log("Hello " + samplePrompt);

	// prompt("Enter anything:");

	// let sampleNullPrompt = prompt("Don't enter anything.");

	// console.log(sampleNullPrompt);
	//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

	//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

	//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

	// function printWelcomeMessage() {
	// 	let firstName = prompt("Enter your first name.");
	// 	let lastName = prompt("Enter your last name.");

	// 	console.log("Hello " + firstName + " " + lastName + "!");
	// 	console.log("Welcome to my page!");
	// };

	// printWelcomeMessage();

// [ S E C T I O N ] Function Naming Conventions
	//1. Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Match 101", "English 101"];
			console.log(courses);
		};

		getCourses();
		
	//2. Avoid generic names to avoid confusion within your code.

		function get(){

			let name = "Jamie";
			console.log(name);

		};

		get();

	//3. Avoid pointless and inappropriate function names.

		function foo(){

			console.log(25 % 5);

		};

		foo();

	//4. Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();

// RETURN STATEMENT
		function returnMe(){
			return true
		};

		// let boolean = returnMe();
		// console.log(boolean);
		console.log(returnMe());


		function returnThis(){
			console.log(false);
		};

		let boolean2 = returnThis();
		console.log(boolean2);
